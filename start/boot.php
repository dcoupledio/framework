<?php 

/**
* Add bootable providers to the Boot queue
**/

add_action( '$boot.queue', function( $queue ){

	$queue([
		new \Decoupled\Boot\HelperBootstrap(),
		new \Decoupled\Boot\ActionBootstrap(),
		new \Decoupled\Boot\ResponseBootstrap(),
		new \Decoupled\Boot\StateProviderBootstrap(),
		new \Decoupled\Boot\TimberBootstrap(),
		new \Decoupled\Boot\BundlerBootstrap(),
	]);
});