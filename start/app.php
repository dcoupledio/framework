<?php

use Decoupled\Core\Application\Application;
use Decoupled\Core\Bootstrap\Bootstrap;
use Decoupled\Core\Bootstrap\BootQueue;
use Decoupled\Core\Decoupled;

/**
* Initialize Core Configurator
**/

$decoupled = new Decoupled();

/**
* Add Core Dependencies
**/

add_action( 'after_setup_theme', function() use( $decoupled ){

	$app       = $decoupled->provider( new Application() );

	$bootstrap = $decoupled->provider( new Bootstrap() ); 

	$bootstrap->setApp( $app );

	$decoupled( $app, $bootstrap );
});

/**
* Bootstrap the app on init
**/

add_action( 'init', function() use( $decoupled ){

	$queue = $decoupled->provider( new BootQueue() );

	do_action( '$boot.queue', $queue );

	$decoupled->boot( $queue );
});

/**
* run the application just before template include
***/

add_action( 'template_include', function( $tpl ) use( $decoupled ){

	$app   = $decoupled->getApp();

	$state = $app['$state'];

	$queue = $app['$action.queue'];

	do_action( '$app.run', $app, $state, $queue );

	echo $decoupled->output( $queue );
}, 1001 );
