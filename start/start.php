<?php

//if not in Wordpress environment, 
//throw exception

if( !defined('ABSPATH') )
{
    throw new \Exception(
        'Cannot run $dcoupled outside of Wordpress environment... yet - try requiring inside your plugin/theme folder'
    );  
}

//if dcoupled is already included, 
//prevent it from running again

if( defined('DCOUPLED') ) return;


$require = function( $path ){

    $ds = DIRECTORY_SEPARATOR;

    $path = dirname(__FILE__).$ds.ltrim($path, $ds);

    $path = str_replace(['/','\\'], $ds, $path);

    require $path.'.php';
};

$require('/app');

$require('/boot');

//define DCOUPLED constant, to prevent from
//running again

define('DCOUPLED', 1); 
