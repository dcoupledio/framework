<?php namespace Decoupled\Boot;

use Decoupled\Core\Bootstrap\BootableInterface;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Routing\Router;
use Decoupled\Core\Routing\RouteBuilderFactory;
use Decoupled\Core\Routing\StateRouteResolver;
use Decoupled\Core\Application\ApplicationState;

class StateProviderBootstrap implements BootableInterface{

	const PROCESS_NAME = '$state.provider';

	public function getProcessName()
	{
		return self::PROCESS_NAME;
	}

	public function boot( Application $app )
	{
		$this->app = $app; 

		//add router services
		$this->addProviders( $app );

		//add listener
		add_action( '$app.run', [ $this, 'run' ], 1000, 3 );

		//define $state service inside template include, so that
		//get body class gives us all the actual classes

		add_action( 'template_include', [ $this, 'setState' ], 1000);
	}

	public function run( $app, $state, $queue )
	{
		//get state based actions, and push them to queue
		$actions = $app['$state.router']->resolve( 
			$state->attributes()->all() 
		);

		//add actions to queue
		$queue( $actions );
	}

	public function addProviders( Application $app )
	{
		$app['$state.router'] = function() use($app){

			$router = new Router;

			$router->setBuilderFactory( new RouteBuilderFactory() );

			$router->setResolver( new StateRouteResolver() );

			$router->setActionFactory( $app['$action.factory'] );

			return $router;
		};
	}

	public function setState( $tpl )
	{
		$this->app['$state'] = function() use( $tpl ){

			$class    = get_body_class( '$app' );

			$defaults = [ 'template' => $tpl ]; 

			$state    = new ApplicationState( $class, $defaults );

			return apply_filters( '$app.state', $state );
		};		
	}

} 