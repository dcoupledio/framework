<?php namespace Decoupled\Boot;

use Decoupled\Core\Bootstrap\BootableInterface;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Response\Response;
use Decoupled\Core\Response\ActionResponseHandler;

class ResponseBootstrap implements BootableInterface{

	const PROCESS_NAME = '$response.boot';

	public function getProcessName()
	{
		return self::PROCESS_NAME;
	}

	public function boot( Application $app )
	{
		//add timber services
		$this->addProviders( $app );
	}

	public function addProviders( Application $app )
	{
		$app['$res'] = function($c){

			$response = new Response( $c['$scope'] );

			$response->setTwig( $c['twig'] );

			return $response;
		};

		$app['$res.action.handler'] = function($c){

			$response = new ActionResponseHandler();

			$response->setHandler( $c['$service.injector'] );

			return $response;
		};
	}	
}