<?php namespace Decoupled\Boot;

use Decoupled\Core\Bootstrap\BootableInterface;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Response\Response;
use Decoupled\Core\DependencyInjection\Scope;
use Timber\Loader;

class TimberBootstrap implements BootableInterface{

	const PROCESS_NAME = '$timber';

	public function getProcessName()
	{
		return self::PROCESS_NAME;
	}

	public function boot( Application $app )
	{
		//add timber services
		$this->addProviders( $app );

		add_action( '$app.run', [ $this, 'run' ], 0, 3 );
	}

	public function run( $app, $state, $queue )
	{
		$factory = $app['$action.factory'];

		//create action that will render plugin or legacy
		//php contents inside @app/legacy.html.twig

		$action = $factory->make([ 
			'$scope', 
			'$state',
			'$res',
			'$template', 
			function( $scope, $state, $res, $template ){

				$tpl = $state->defaults()->get('template');

				$scope['content'] = $template->contents( $tpl );

				$template = apply_filters( '$template.legacy', '@app.bundle/legacy.html.twig' );

				return $res( $template, $scope );
		}]);

		//add action to the queue

		$queue([ $action ]);
	}	

	public function addProviders( Application $app )
	{
		$app['twig'] = function(){

            $timber = new Loader();

			return $timber->get_twig();
		};

		$app['$scope'] = function(){

			$data = \Timber::get_context();

			return new Scope( $data );
		};

		$app['$post'] = function(){

			return new \TimberPost();
		};

		$app['$posts'] = function(){

			return \Timber::get_posts();
		};

		$app['$author'] = function(){

			global $wp_query;

			if( $author = @$wp_query->query_vars['author'] )
			{
				return new \TimberUser( $author );
			}

			return false;
		};

		$app['$menu'] = function(){

			return function( $id = null ){
				return new \TimberMenu($id);
			};
		};
	}	
}