<?php namespace Decoupled\Boot;

use Decoupled\Core\Bootstrap\BootableInterface;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Bundle\Bundler;
use Decoupled\Core\Bundle\BundleStore;
use Decoupled\Core\Bundle\BundleEnqueue;
use Decoupled\Core\Bundle\BundleFactory;

class BundlerBootstrap implements BootableInterface{

	const EVENT_REGISTER = '$bundles.register';

	const PROCESS_NAME = '$bundle.enqueue';

	public function getProcessName()
	{
		return self::PROCESS_NAME;
	}

	public function boot( Application $app )
	{
		$this->addProviders( $app, $app['$d'] );

		do_action( 
			self::EVENT_REGISTER, 
			$app['$bundle.store'], 
			$app['$bundle.factory'] 
		);

		add_action( 'wp_loaded', function() use($app){

			$app['$bundle']->init( $app );
		});
	}

	public function addProviders( Application $app, $d )
	{
		$app['$bundle'] = function($a) use( $d ){

			$bundlr = $d->provider( 
				new Bundler() 
			);

			$bundlr->setStore( $a['$bundle.store'] );

			return $bundlr;
		};

		$app['$bundle.store'] = function() use($d){
			
			return $d->provider( 
				new BundleStore() 
			);
		};

		$app['$bundle.enqueue'] = function() use($d){

			return new BundleEnqueue();
		};

		$app['$bundle.factory'] = function() use($d){

			return new BundleFactory();
		};
	}
} 