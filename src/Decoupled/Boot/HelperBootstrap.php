<?php namespace Decoupled\Boot;

use Decoupled\Core\Bootstrap\BootableInterface;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Template\TemplateHelper;
use Symfony\Component\ClassLoader\ClassLoader;

class HelperBootstrap implements BootableInterface{

	const PROCESS_NAME = '$helper.boot';

	public function getProcessName()
	{
		return self::PROCESS_NAME;
	}

	public function boot( Application $app )
	{
		$this->addProviders( $app );
	}

	public function addProviders( Application $app )
	{
		$app['$template'] = function(){
			return new TemplateHelper();
		};

		$app['$autoloader'] = function(){
			return new ClassLoader();
		};

		$app['$dot.format'] = function(){
			return function( $str ){
				$str = str_replace("_", ".", $str);

				$str = preg_split('/(?=[A-Z])/', $str, -1, PREG_SPLIT_NO_EMPTY);

				$str = strtolower( implode(".", $str) );

				return $str;
			};
		};

		$app['$archive'] = function($c){

			$archive = new \stdClass();

			$archive->type = false;

			if ( is_day() ) 
			{
				$archive->type  = 'day';

				$archive->date  = get_the_date( 'D M Y' );
			} 
			else if ( is_month() ) 
			{
				$archive->type = 'month';

				$archive->date = get_the_date( 'M Y' ); 
			} 
			else if ( is_year() ) 
			{
				$archive->type = 'year';

				$archive->date = get_the_date( 'Y' );
			} 
			else if ( is_tag() ) 
			{
				$archive->type = 'tag';

				$archive->title = single_tag_title( '', false );
			} 
			else if ( is_category() ) 
			{
				$archive->type = 'category';

				$archive->title = single_cat_title( '', false );
			} 
			else if ( is_post_type_archive() ) 
			{
				$archive->type = get_post_type();

				$archive->title = post_type_archive_title( '', false );
			}

			$archive->posts = $c['$posts'];

			return $archive;
		};
	}
} 