<?php namespace Decoupled\Boot;

use Decoupled\Core\Bootstrap\BootableInterface;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Action\ActionFactory;
use Decoupled\Core\Action\ActionQueue;
use Decoupled\Core\DependencyInjection\ServiceInjector;

class ActionBootstrap implements BootableInterface{

	const PROCESS_NAME = '$action.boot';

	public function getProcessName()
	{
		return self::PROCESS_NAME;
	}

	public function boot( Application $app )
	{
		$this->addProviders( $app );
	}

	public function addProviders( Application $app )
	{
		$app['$action.factory'] = function(){
			return new ActionFactory();
		};

		$app['$action.queue'] = function(){
			return new ActionQueue();
		};

		$app['$service.injector'] = function($c){

			$injector = new ServiceInjector();

			$injector->setApp( $c );

			return $injector;
		};
	}
} 