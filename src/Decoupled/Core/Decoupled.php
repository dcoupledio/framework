<?php namespace Decoupled\Core;

use Decoupled\Core\Application\Application;
use Decoupled\Core\Bundle\Bundler;
use Decoupled\Core\Bundle\BundleStore;
use Decoupled\Core\Provider\ProviderException;
use Decoupled\Core\Bootstrap\Bootstrap;
use Decoupled\Core\Bootstrap\BootQueue;
use Decoupled\Core\Action\ActionQueue;

class Decoupled{

	protected $app;

	protected $bootstrap;

	public static function provider( $provider )
	{
		return apply_filters( $provider::PROVIDER, $provider );
	}

	public function __invoke( Application $app, Bootstrap $boot )
	{
		$this->addProviders( $app );

		$this->setApp( $app );

		$this->setBootstrap( $boot );

		return $this;
	}

	public function addProviders( Application $app )
	{
		$d = $this;

		$app['$d'] = function() use($d){
			return $d;
		};
	}

	public function setApp( Application $app )
	{
		$this->app = $app;

		return $this;
	}

	public function getApp()
	{
		return $this->app;
	}

	public function setBootstrap( Bootstrap $bootstrap )
	{
		$this->bootstrap = $bootstrap;

		return $this;
	}

	public function getBootstrap()
	{
		return $this->bootstrap;
	}

	public function boot( BootQueue $queue )
	{
		$this->bootstrap->start( $queue );

		return $this;
	}

	public function output( ActionQueue $queue )
	{
		$app = $this->getApp();

		return $app['$res.action.handler']
		    ->handle( $queue )
		    ->getOutput()
		    ->render();
	}

}