<?php namespace Decoupled\Core\Template;


class TemplateHelper{

	/**
	* use with caution, as including Wordpress templates prematurely
	* can break, or have unexpected results if dependencies not loaded
	**/

	public function contents( $tpl )
	{
		ob_start();

		include( $tpl );

		return ob_get_clean();
	}

}