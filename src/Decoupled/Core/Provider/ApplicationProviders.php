<?php namespace Decoupled\Core\Provider;

use Decoupled\Core\Application\ApplicationAddon;

class ApplicationProviders extends ApplicationAddon{

	public function getName()
	{
		return 'providers';
	}

	public function execute()
	{
		$callback = $this->getCallback();

		return $callback( $this->getApp() );
	}
}