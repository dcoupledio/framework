<?php namespace Decoupled\Core\Bundle;


class BundleStore{

	const PROVIDER = 'bundle.store';

	protected $bundles = [];

	public function __invoke( array $bundles )
	{
		foreach( $bundles as $bundle )
		{
			$this->add( $bundle );
		}

		return $this;
	}

	public function add( BundleInterface $bundle )
	{
		$this->bundles[ $bundle->getName() ] = $bundle;

		return $this;
	}

	public function get( $name )
	{
		return $this->bundles[ $name ];
	}

	public function remove( $name )
	{
		unset( $this->bundles[$name] );

		return $this;
	}

	public function all()
	{
		return $this->bundles;
	}

	public function map( $callback )
	{
		array_map( $callback, $this->all() );

		return $this;
	}

}