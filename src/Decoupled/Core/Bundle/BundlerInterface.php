<?php namespace Decoupled\Core\Bundle;

use Decoupled\Core\Application;

interface BundlerInterface{

	public function setStore( BundleStore $bundles );

	public function getStore();
}