<?php namespace Decoupled\Core\Bundle;

use Decoupled\Core\Routing\Router;
use Decoupled\Core\Application\Application;

class BundleDynamic implements BundleInterface{

	protected $name;

	protected $dir;

	protected $viewDir;

	protected $assetsCallback;

	protected $routesCallback;

	protected $providersCallback;

	public function getName()
	{
		return $this->name;
	}

	public function setName( $name )
	{
		$this->name = $name;

		return $this;
	}

	public function getDir()
	{
		return $this->dir;
	}

	public function setDir( $dir )
	{
		$this->dir = $dir;

		return $this;
	}

	public function getViewDir()
	{
		return $this->viewDir;
	}

	public function setViewDir( $dir )
	{
		$this->viewDir = $dir;

		return $this;
	}

	public function setAssets()
	{
		if(!$this->assetsCallback) return;

		return call_user_func( $this->assetsCallback );
	}

	public function setAssetsCallback( callable $callback = null )
	{
		$this->assetsCallback = $callback;

		return $this;
	}

	public function setRoutes( Router $router )
	{
		if(!$this->routesCallback) return;

		return call_user_func( $this->routesCallback, $router );
	}

	public function setRoutesCallback( callable $callback = null )
	{
		$this->routesCallback = $callback;

		return $this;
	}

	public function setProviders( Application $app )
	{
		if(!$this->providersCallback) return;

		return call_user_func( $this->providersCallback, $app );
	}

	public function setProvidersCallback( callable $callback = null )
	{
		$this->providersCallback = $callback;

		return $this;
	}

}