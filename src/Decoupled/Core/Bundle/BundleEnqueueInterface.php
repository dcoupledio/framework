<?php namespace Decoupled\Core\Bundle;

interface BundleEnqueueInterface{

	public function enqueue( BundleInterface $bundle );
}