<?php namespace Decoupled\Core\Bundle;

class BundleEnqueue implements BundleEnqueueInterface{

	public function enqueue( BundleInterface $bundle )
	{
		add_action('$bundle.enqueue', function($queue){

			$queue([ $bundle ]);
		});
	}
}