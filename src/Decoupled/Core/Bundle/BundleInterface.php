<?php namespace Decoupled\Core\Bundle;

use Decoupled\Core\Routing\Router;
use Decoupled\Core\Application\Application;

interface BundleInterface{

	public function getName();

	public function getDir();

	public function getViewDir();

	public function setAssets();

	public function setRoutes( Router $router );

	public function setProviders( Application $app );

}