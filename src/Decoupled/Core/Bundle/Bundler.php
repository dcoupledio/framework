<?php namespace Decoupled\Core\Bundle;

use InvalidArgumentException;
use Decoupled\Core\Bundle\BundleInterface;
use Decoupled\Core\Bootstrap\Bootstrap;
use Decoupled\Core\Bootstrap\BootableInterface;
use Decoupled\Core\Bundle\BundleStore;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Routing\ApplicationRoutes;
use Decoupled\Core\Assets\ApplicationAssets;
use Decoupled\Core\Provider\ApplicationProviders;
use Decoupled\Core\View\ApplicationViewPaths;
use Decoupled\Core\DependencyInjection\ApplicationAutoloadAddon;

class Bundler implements BundlerInterface{

	const PROVIDER = 'bundler.provider';

	const PROCESS  = 'bundler';

	public function setStore( BundleStore $bundles )
	{
		$this->store = $bundles;

		return $this;
	}

	public function getStore()
	{
		return $this->store;
	}

	public function init( Application $app )
	{
		$this->app = $app;

		$bundles = $this->getStore();

		$bundles

		    ->map( [$this, 'prefixes'] )

		    ->map( [$this, 'providers'] )

		    ->map( [$this, 'templates'] )

		    ->map( [$this, 'assets'] )

		    ->map( [$this, 'routes'] );
	}

	public function prefixes( $bundle )
	{
		$app = $this->app;

		$app->uses(
			new ApplicationAutoloadAddon(function( $autoloader ) use( $bundle ){

				$dirs = $bundle->getDir();

				foreach( $dirs as $prefix => $dir )
				{
					$autoloader->addPrefix( $prefix, $dir );
				}

				return $autoloader;
			})
		);
	}

	public function templates( $bundle )
	{
		$app = $this->app;

		$app->uses(
			new ApplicationViewPaths(function( $loader, $format ) use( $bundle ){

				$paths = $bundle->getViewDir();

				foreach( $paths as $namespace => $path )
				{
					$loader->addPath( $path, $format($namespace) );
				}
			})
		);
	}

	public function assets( $bundle )
	{
		$app = $this->app;

		$app->uses( 
			new ApplicationAssets(function() use( $bundle ){
				return $bundle->setAssets();
			})
		);

		return $this;
	}

	public function providers( $bundle )
	{
		$app = $this->app;

		$app->uses(
			new ApplicationProviders(function( $app ) use( $bundle ){
				return $bundle->setProviders( $app );
			})
		);

		return $this;		
	}

	public function routes( $bundle )
	{
		$app = $this->app;

		$app->uses(
			new ApplicationRoutes(function( $router ) use( $bundle ){
				return $bundle->setRoutes( $router ); 
			})
		);

		return $this;	
	}	
}