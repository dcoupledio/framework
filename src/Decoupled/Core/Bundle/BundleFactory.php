<?php namespace Decoupled\Core\Bundle;

class BundleFactory{

	public function make( $name, array $properties )
	{
		$bundle = new BundleDynamic();		

		return $bundle

		    ->setName( $name )

		    ->setDir( $properties['dir'] )

		    ->setViewDir( $properties['viewDir'] )

		    ->setAssetsCallback( @$properties['assets'] )

		    ->setRoutesCallback( @$properties['routes'] )

		    ->setProvidersCallback( @$properties['providers'] );    
	}

}