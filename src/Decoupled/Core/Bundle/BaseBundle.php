<?php namespace Decoupled\Core\Bundle;

use Decoupled\Core\Application\Application;
use Decoupled\Core\Routing\Router;

abstract class BaseBundle implements BundleInterface{

    public abstract function getName();   

    public abstract function setRoutes( Router $router );

    public abstract function getDir();

    public abstract function getViewDir();

    public function setAssets()
    {
        return;
    }


    public function setProviders( Application $app )
    {
        return;        
    }

}