<?php namespace Decoupled\Core\DependencyInjection;

use Decoupled\Core\Application\Application;
use Decoupled\Core\Action\Action;
use Decoupled\Core\Action\ActionInvokerInterface;
use Decoupled\Core\DependencyInjection\UndefinedDependencyException;

class ServiceInjector implements ActionInvokerInterface{

	protected $app;

	protected $actionConverter;

	public function setApp( Application $app )
	{
		$this->app = $app;

		return $this;
	}

	public function getApp()
	{
		return $this->app;
	}

	public function resolveDependencies( array $deps )
	{
		$resolved = [];

		$app = $this->getApp();

		foreach( $deps as $dep )
		{
			$name = $dep; 

			try
			{
				$resolved[] = $app[$name];
			}
			catch( \Exception $e )
			{
				throw new UndefinedDependencyException(
					"Attempted to request undefined provider ".$name
				);
			}
		}

		return $resolved;
	}

	public function __invoke( Action $action )
	{
		return $this->invoke( $action );
	}

	public function invoke( Action $action )
	{
		$params = $this->resolveDependencies( 
			$action->getDeps() 
		);

		$callable = $action->getCallable();

		return call_user_func_array( $callable, $params );
	}

}