<?php namespace Decoupled\Core\DependencyInjection;

use Illuminate\Support\Collection;

class Scope extends Collection{

    public function getData()
    {
        return $this->all();
    }

}