<?php namespace Decoupled\Core\DependencyInjection;

use Decoupled\Core\Application\ApplicationAddon;

class ApplicationAutoloadAddon extends ApplicationAddon{

	public function getName()
	{
		return 'autoload';
	}

	public function execute()
	{
		$callback   = $this->getCallback();

		$app        = $this->getApp();

		$autoloader = $callback( $app['$autoloader'] );

		$autoloader->register();
	}	

}