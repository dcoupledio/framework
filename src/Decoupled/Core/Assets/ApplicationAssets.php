<?php namespace Decoupled\Core\Assets;

use Decoupled\Core\Application\ApplicationAddon;

class ApplicationAssets extends ApplicationAddon{

	public function getName()
	{
		return 'assets';
	}

	public function execute()
	{
		$callback = $this->getCallback();

		add_action('wp_enqueue_scripts', $callback);
	}
}