<?php namespace Decoupled\Core\Bootstrap;

class BootQueue{

	protected $providers = [];

	const PROVIDER = 'bootstrap.queue.provider';

	public function __invoke( $providers )
	{
		if( !is_array($providers) ) $providers = func_get_args();

		foreach( $providers as $provider )
		{
			$this->add( $provider );
		}

		return $this;
	}

	public function add( BootableInterface $provider )
	{
		$this->providers[ $provider->getProcessName() ] = $provider;

		return $this;
	}

	public function remove( $name )
	{
		unset( $this->providers[$name] );

		return $this;
	}

	public function all()
	{
		return $this->providers;
	}

}