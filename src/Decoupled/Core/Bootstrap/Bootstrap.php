<?php namespace Decoupled\Core\Bootstrap;

use Decoupled\Core\Application\Application;

class Bootstrap implements BootstrapInterface{

	protected $app;

	const PROVIDER = 'bootstrap.provider';

	public function setApp( Application $app )
	{
		$this->app = $app;

		return $this;
	}

	public function getApp()
	{
		return $this->app;
	}	

	public function start( BootQueue $queue )
	{
		$providers = $queue->all();

		$app       = $this->getApp();

		foreach( $providers as $process => $provider )
		{
			do_action( $process.".boot.before", $app );

			$provider->boot( 
				$this->getApp() 
			);

			do_action( $process.".boot.after", $app );
		}

		return $this;
	}

	public function __invoke( BootQueue $queue )
	{
		$this->start( $queue );
	}
}
