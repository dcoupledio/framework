<?php namespace Decoupled\Core\Bootstrap;

use Decoupled\Core\Application\Application;

interface BootableInterface{

	public function boot( Application $app );

	public function getProcessName();
}