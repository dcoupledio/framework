<?php namespace Decoupled\Core\Bootstrap;

use Decoupled\Core\Bootstrap\Bootstrap;
use Decoupled\Core\Application\Application;

class BootstrapFactory{

	public static function make( Application $app )
	{
		$bootstrap = new Bootstrap();

		$bootstrap->setApp( $app );

		return $bootstrap;
	}

}