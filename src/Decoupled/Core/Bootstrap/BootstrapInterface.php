<?php namespace Decoupled\Core\Bootstrap;

use Decoupled\Core\Application\Application;

interface BootstrapInterface{

	public function start( bootQueue $queue );

	public function setApp( Application $app );

	public function getApp();

}