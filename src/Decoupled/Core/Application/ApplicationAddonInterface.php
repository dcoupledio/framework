<?php namespace Decoupled\Core\Application;

use Decoupled\Core\Application\Application;

interface ApplicationAddonInterface{

	public function getName();

	public function getCallback();

	public function execute();

	public function setApp( Application $app );
}