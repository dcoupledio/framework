<?php namespace Decoupled\Core\Application;

class Application extends \Pimple\Container{
	
	const PROVIDER = 'container.provider';

	public function uses( $addons )
	{
		if( !is_array($addons) ) $addons = func_get_args();

		$addons = array_values($addons);

		for($i = 0; $i < count($addons); $i++ )
		{
			$this->useAddon( $addons[$i] );
		}

		return $this;
	}

	public function useAddon( ApplicationAddonInterface $addon )
	{
		$addon->setApp( $this )->execute();

		return $this;
	}

	public function offsetGet( $service )
	{
		if( !isset($this[ $service ]) && strpos($service, '$') !== 0 )
		{
			$service = '$'.$service;
		}

		return parent::offsetGet( $service );
	}
}