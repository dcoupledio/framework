<?php namespace Decoupled\Core\Application;

use Decoupled\Core\Application\AddonInterface;

abstract class ApplicationAddon implements ApplicationAddonInterface{

	protected $callback;

	protected $app;

	public function __construct( callable $callback )
	{
		$this->callback = $callback;
	}

	public function getCallback()
	{
		return $this->callback;
	}

	public function execute()
	{
		return call_user_func_array(
			$this->getCallback(), 
			func_get_args()
		);
	}

	public function setApp( Application $app )
	{
		$this->app = $app;

		return $this;
	}

	public function getApp()
	{
		return $this->app;
	}

	abstract public function getName();

}