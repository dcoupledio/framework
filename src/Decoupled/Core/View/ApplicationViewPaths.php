<?php namespace Decoupled\Core\View;

use Decoupled\Core\Application\ApplicationAddon;

class ApplicationViewPaths extends ApplicationAddon{

	public function getName()
	{
		return 'viewPaths';
	}

	public function execute()
	{
		$callback = $this->getCallback();

		$app      = $this->getApp();

		return $callback( $app['twig']->getLoader(), $app['$dot.format'] );
	}
}