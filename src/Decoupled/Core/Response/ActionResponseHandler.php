<?php namespace Decoupled\Core\Response;

use Decoupled\Core\Action\ActionQueue;
use Decoupled\Core\Action\ActionInvokerInterface;
use Decoupled\Core\Response\Response;

class ActionResponseHandler{

	protected $output;

	protected $invoker;

	public function getOutput()
	{
		return $this->output;
	}

	public function setOutput( Response $response )
	{
		$this->output = $response;

		return $this;
	}

	public function setHandler( ActionInvokerInterface $invoker )
	{
		$this->invoker = $invoker;

		return $this;
	}

	public function getHandler()
	{
		return $this->invoker;
	}

	public function handle( ActionQueue $queue )
	{
		$output = null;

		foreach( $queue->all() as $action )
		{
			$resp = $this->getHandler()->invoke( $action );

			if( $resp instanceof Response )
			{
				$this->setOutput( $resp ); 	
			}
		}

		return $this;
	}

}