<?php namespace Decoupled\Core\Response;

use Decoupled\Core\DependencyInjection\Scope;

class Response{

	protected $template;

	protected $scope;

	protected $twig;

	public function __construct( Scope $scope )
	{
		$this->scope = $scope;
	}

	public function __invoke( $template, $scope = [] )
	{
		return $this->view( $template )->with( $scope );
	}

	public function __toString()
	{
		return $this->render();
	}

	public function setTwig( $twig )
	{
		$this->twig = $twig;
	}

	public function getTwig()
	{
		return $this->twig;
	}

	public function view( $template )
	{
		$this->template = $template;

		return $this;
	}

	public function with( $key, $value = null )
	{
		if( is_array($key) || ($key instanceof Scope) ) 
		{
			$values = $key;

			foreach( $values as $k => $v )
			{
				$this->scope[ $k ] = $v;
			}

			return $this;
		}

		$this->scope[$key] = $value;

		return $this;
	}

	public function render()
	{
		return $this
		    ->getTwig()
		    ->render( 
		    	$this->template,  
		    	$this->scope->all()
		    );
	}

}