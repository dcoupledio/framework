<?php namespace Decoupled\Core\Action;

class ActionQueue{

	const PROVIDER = 'action.queue';

	protected $actions = [];

	public function __invoke( array $actions )
	{
		foreach( $actions as $action )
		{
			$this->add( $action );
		}

		return $this;
	}

	public function add( ActionInterface $action )
	{
		$this->actions[] = $action;

		return $this;
	}

	public function all()
	{
		return $this->actions;
	}

	public function map( $callback )
	{
		array_map( $callback, $this->all() );

		return $this;
	}

}