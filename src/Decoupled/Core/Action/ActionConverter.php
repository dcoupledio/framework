<?php namespace Decoupled\Core\Action;

use ReflectionMethod;
use ReflectionFunction;

class ActionConverter{

	const METHOD_SEPARATOR = '@';

	public static function parse( $action )
	{
		$deps = [];

		$method = $action; 

		if( is_array($action) )
		{
			$method = array_pop( $action );

			$deps   = $action;
		}

		$action = self::convert( $method );

		if( empty($deps) ) $deps = self::getParameterNames( $action );

		return [ $action, $deps ];
	}

	public static function getParameterNames( $action )
	{
		if( is_array( $action ) )
		{
			$method = new ReflectionMethod( $action[0], $action[1] );
		}
		else
		{
			$method = new ReflectionFunction( $action );
		}

		$params = [];

		foreach( $method->getParameters() as $param )
		{
			$params[] = $param->getName();
		}

		return $params;
	}

	public static function convert( $action )
	{
		if( is_callable($action) ) return $action;

		if( is_string($action) && strpos( $action, self::METHOD_SEPARATOR ) !== false )
		{
			$action = explode( self::METHOD_SEPARATOR, $action );

			$action[0] = new $action[0];

			return $action;
		}

		throw new InvalidActionException('Invalid action provided ' .$action);
	}

}