<?php

require('../vendor/autoload.php');

use phpunit\framework\TestCase;

use Decoupled\Core\Action\Action;

use Decoupled\Core\Action\ActionFactory;

use Decoupled\Core\Action\Tests\BindObject;

use Decoupled\Core\Action\Tests\ActionInvoker;

class ActionTest extends TestCase
{
    public function __construct()
    {
        $this->invoker = new ActionInvoker();

        $this->bindObject = new BindObject();

        $this->factory = new ActionFactory();
    }

    public function testCanBindClass()
    {
        $action = new Action(function( $param ){

            return $this->getValue();
        });

        $bind = $this->bindObject;

        $action->as( $bind );

        $this->assertEquals( 
            $bind->getValue(), 
            $this->invoker->invoke( $action )
        );

        return $action;
    }

    /**
    * @depends testCanBindClass
    **/

    public function testCanAccessPrivateMethod( $action )
    {
        $bind = new BindObject();

        $action->setCallable(function(){

            return $this->getPrivateValue();
        });

        $bind->bind( $action );

        $this->assertTrue( $this->invoker->invoke($action) );
    }

    /**
    * @depends testCanBindClass
    **/

    public function testCanInvokeinternally( $action )
    {
        $action->setInvoker( $this->invoker );

        $this->assertEquals( $action(), $this->bindObject->getValue() );
    }

    public function testCanCreateInvokeableActionWithFactory()
    {
        $action = $this->factory->make(function(){

            return;
        });

        $this->assertTrue( $action instanceof Decoupled\Core\Action\Action );

        $action = $this
            ->factory
            ->setInvoker( $this->invoker )
            ->make(function(){
            
                return 1;
        });

        $this->assertEquals( $action(), 1 );
    }
}