<?php namespace Decoupled\Core\Action\Tests;

use Decoupled\Core\Action\ActionInvokerInterface;
use Decoupled\Core\Action\Action;

class ActionInvoker implements ActionInvokerInterface{
    
    public function invoke( Action $action )
    {
        $callback = $action->getCallable();

        $deps = $action->getDeps();

        return call_user_func_array( $callback, $deps );
    }

}