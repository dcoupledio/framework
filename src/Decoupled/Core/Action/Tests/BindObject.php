<?php namespace Decoupled\Core\Action\Tests;

class BindObject{

    private function getPrivateValue()
    {
        return true;
    }

    public function getValue()
    {
        return true;
    }

    public function bind( $action )
    {
        return $action->as( $this );
    }

}