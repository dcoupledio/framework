<?php namespace Decoupled\Core\Action;

interface ActionFactoryInterface{

	public function make( $action );

}