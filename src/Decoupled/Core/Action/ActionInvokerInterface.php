<?php namespace Decoupled\Core\Action;

interface ActionInvokerInterface{

	public function invoke( Action $action );

}