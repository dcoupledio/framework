<?php namespace Decoupled\Core\Action;

use Decoupled\Core\Action\ActionConverter;

class Action implements ActionInterface, InvokeableActionInterface{

	protected $action;

	public function __construct( $action )
	{
		$this->set( $action );
	}

	public function __invoke()
	{
		return $this->getInvoker()->invoke( $this );
	}

	public function getDeps()
	{
		return @$this->action[1] ?: [];
	}

	public function getCallable()
	{
		return $this->action[0];
	}

	public function setCallable( callable $action )
	{
		$this->action[0] = $action;

		return $this;
	}

	public function as( $object )
	{
		$action = $this->getCallable();

		if( $action instanceof \Closure )
		{
			$action = $action->bindTo( $object, $object );

			$this->setCallable( $action );
		}

		return $this;
	}

	public function setInvoker( ActionInvokerInterface $invoker )
	{
		$this->invoker = $invoker;

		return $this;
	}

	public function getInvoker()
	{
		return $this->invoker;
	}

	protected function set( $action )
	{
		$this->action = ActionConverter::parse( $action );
	}

}