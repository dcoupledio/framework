<?php namespace Decoupled\Core\Action;

interface InvokeableActionInterface{

    public function setInvoker( ActionInvokerInterface $invoker );

    public function getInvoker();

    public function __invoke();
}