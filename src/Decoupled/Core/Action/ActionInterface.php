<?php namespace Decoupled\Core\Action;

interface ActionInterface{

	public function getDeps();

	public function getCallable();

}