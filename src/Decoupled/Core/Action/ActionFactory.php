<?php namespace Decoupled\Core\Action;

use Decoupled\Core\Action\ActionConverter;

class ActionFactory implements ActionFactoryInterface{

    protected $invoker;

	public function make( $action )
	{
		$action = new Action($action);

        if( $invoker = $this->getInvoker() )
        {
            $action->setInvoker( $invoker );
        }

        return $action;
	}

    public function setInvoker( ActionInvokerInterface $invoker )
    {
        $this->invoker = $invoker;

        return $this;
    }

    public function getInvoker()
    {
        return $this->invoker;
    }

}