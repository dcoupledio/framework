<?php namespace Decoupled\Core\Routing;

use Decoupled\Core\Action\ActionFactoryInterface;

interface RouteBuilderFactoryInterface{

	public function make( $classes, ActionFactoryInterface $action );
}