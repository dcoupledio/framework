<?php namespace Decoupled\Core\Routing;

interface RouteBuilderInterface{

	public function uses( $callback );

	public function not( $classes );

	public function when( $classes );

	public function addClasses( array $classes );

	public function setClasses( array $classes );

	public function excludeClasses( array $classes );

	public function setAction( $callback );

	public function getAction();

	public function setName( $name );

	public function getName();
}