<?php namespace Decoupled\Core\Routing;

use Decoupled\Core\Bootstrap\BootableInterface;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Routing\Router;
use Decoupled\Core\Routing\RouteBuilderFactory;
use Decoupled\Core\Routing\RouteBuilder;

class StateRouteResolver{

	public static function resolveActions( array $state, array $routes )
	{
		$actions = [];

		foreach( $routes as $route )
		{
			$actions[] = self::resolveAction( $state, $route );
		}

		return array_filter( $actions );
	}

	public static function resolveAction( array $state, RouteBuilder $route )
	{
		$excluded = $route->getExcludedClasses();

		if( self::isState( $state, $excluded ) )
		{
			return false;
		}

		$any = $route->getAnyClasses();

		if( self::isState( $state, $any, true ) )
		{
			return true;
		}

		$classes = $route->getClasses();

		if( self::isState( $state, $classes ) )
		{
			return $route->getAction();
		}

		return false;
	}

	public static function isState( array $state, array $classes, $any = false )
	{
		if( count($state) < count($classes) || empty($classes) ) 
		{
			return false;
		}

		foreach( $classes as $cls )
		{
			if( !in_array( $cls, $state ) && !$any )
			{
				return false;
			}

			if( in_array( $cls, $state ) && $any )
			{
				return true;
			}
		}

		return true;
	}

}