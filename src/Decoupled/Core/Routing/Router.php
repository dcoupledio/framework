<?php namespace Decoupled\Core\Routing;

use Decoupled\Core\Routing\RouteBuilderFactoryInterface;
use Decoupled\Core\Routing\RouteBuilderInterface;
use Decoupled\Core\Routing\StateRouteResolver;
use Decoupled\Core\Action\ActionFactoryInterface;

class Router{

    protected $routes = [];

    protected $factory;

    protected $resolver;

    protected $actionFactory;

    public function __invoke( $name )
    {
        return $this->get( $name );
    }

    public static function parseClassList( $classes )
    {
        if( is_array($classes) ) return $classes;

        return explode(" ", $classes);
    }

    public function setBuilderFactory( RouteBuilderFactoryInterface $factory )
    {
        $this->factory = $factory;

        return $this;
    }

    public function setActionFactory( ActionFactoryInterface $factory )
    {
        $this->actionFactory = $factory;

        return $this;
    }

    public function getActionFactory()
    {
        return $this->actionFactory;
    }

    public function getBuilderFactory()
    {
        return $this->factory;
    }

    public function setResolver( StateRouteResolver $resolver )
    {
        $this->resolver = $resolver;

        return $this;
    }

    public function getResolver()
    {
        return $this->resolver;
    }

    public function get( $name )
    {
        if( !isset($this->routes[$name]) )
        {
            $route = $this->make($name);

            $this->add( $route );
        }

        return $this->routes[$name];
    }

    public function make( $name, $classes = [] )
    {
        $builder = $this
            ->getBuilderFactory()
            ->make( $classes, $this->getActionFactory() );

        $builder->setName( $name );

        return $builder;
    }

    public function when( $classes )
    {
        $route = $this
            ->getBuilderFactory()
            ->make( $classes, $this->getActionFactory() );

        $this->add( $route );

        return $route;
    }

    public function resolve( array $state )
    {
        return $this
            ->getResolver()
            ->resolveActions( $state, $this->all() );
    }

    public function add( RouteBuilderInterface $route )
    {
        if( $name = $route->getName() )
        {
            $this->routes[ $name ] = $route;
        }
        else
        {
            $this->routes[] = $route;
        }

        return $this;
    }

    public function remove( $name )
    {
        foreach( $this->routes as $key => $route )
        {
            if( $route->getName() === $name )
            {
                unset( $this->routes[ $key ] );

                break;
            }
        }

        return $this;
    }

    public function all()
    {
        return $this->routes;
    }

} 