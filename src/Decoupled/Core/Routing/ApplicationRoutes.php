<?php namespace Decoupled\Core\Routing;

use Decoupled\Core\Application\ApplicationAddon;

class ApplicationRoutes extends ApplicationAddon{

	public function getName()
	{
		return 'routeCollection';
	}

	public function execute()
	{
		$callback = $this->getCallback();

		$app = $this->getApp();

		return $callback( $app['$state.router'] );
	}
}