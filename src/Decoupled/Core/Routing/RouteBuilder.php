<?php namespace Decoupled\Core\Routing;

use Decoupled\Core\Routing\Router;
use Decoupled\Core\Action\ActionFactoryInterface;

class RouteBuilder implements RouteBuilderInterface{
	
	protected $classes = [];

	protected $excludedClasses = [];

	protected $anyClasses = [];

	protected $action  = null;

	protected $name = null;

	protected $actionFactory;

	public function __construct( $classes = [], $name = null )
	{
		if( !empty($classes) )
		{
			$this->when( $classes );
		}

		if( $name )
		{
			$this->setName( $name );
		}
	}

	public function uses( $callback )
	{
		$this->setAction( $callback );

		return $this;
	}

	public function not( $classes )
	{
		$classes = Router::parseClassList( $classes );

		return $this->excludeClasses( $classes );
	}	

	public function when( $classes )
	{
		$classes = Router::parseClassList( $classes );

		return $this->addClasses( $classes );
	}

	public function any( $classes )
	{
		$classes = Router::parseClassList( $classes );

		return $this->addAnyClasses( $classes );
	}

	public function addAnyClasses( array $classes )
	{
		$this->anyClasses = array_merge( $this->anyClasses, $classes );

		return $this;
	}

	public function getAnyClasses()
	{
		return $this->anyClasses;
	}

	public function addClasses( array $classes )
	{
		$this->classes = array_merge(
			$this->classes,
			$classes
		);

		return $this;
	}

	public function setClasses( array $classes )
	{
		$this->classes = $classes;

		return $this;
	}

	public function getClasses()
	{
		return $this->classes;
	}

	public function excludeClasses( array $classes )
	{
		$this->excludedClasses = $classes;

		return $this;
	}

	public function getExcludedClasses()
	{
		return $this->excludedClasses;
	}

	public function setAction( $callback )
	{
		$this->action = $this
		    ->getActionFactory()
		    ->make( $callback );

		return $this;
	}

	public function getAction()
	{
		return $this->action;
	}

	public function setName( $name )
	{
	    $this->name = $name;

	    return $this;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setActionFactory( ActionFactoryInterface $factory )
	{
		$this->actionFactory = $factory;

		return $this;
	}

	public function getActionFactory()
	{
		return $this->actionFactory;
	}
}