<?php namespace Decoupled\Core\Routing;

use Decoupled\Core\Routing\RouteBuilder;
use Decoupled\Core\Action\ActionFactoryInterface;

class RouteBuilderFactory implements RouteBuilderFactoryInterface{

	public function make( $classes, ActionFactoryInterface $action )
	{
		$route = new RouteBuilder( $classes );

		$route->setActionFactory( $action );

		return $route;
	}
}